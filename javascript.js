function initMap() {
    var uluru = {lat: 50.065197, lng: 19.941683};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: uluru,
        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}

$(function() {
    $("#teambutton").click(function () {
        $('html, body').animate({
            scrollTop: $("#team").offset().top -50
        },1000);
    });
    $("#workbutton").click(function () {
        $('html, body').animate({
            scrollTop: $("#work").offset().top -50
        }, 1000);
    });
    $("#pricebutton").click(function () {
        $('html, body').animate({
            scrollTop: $("#price").offset().top -50
        }, 1000);
    });
    $("#contactbutton").click(function () {
        $('html, body').animate({
            scrollTop: $("#contact").offset().top -50
        }, 1000);
    });
    $("#up").click(function () {
        $('html, body').animate({
            scrollTop: $("#parallax").offset().top -50
        }, 800);
    });
    initMap();
    changeSlide();
});
